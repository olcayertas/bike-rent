//
//  ViewModels.swift
//  Bike Rent
//
//  Created by Olcay Ertaş on 10/09/2017.
//  Copyright © 2017 Saitama. All rights reserved.
//

import Foundation
import MapKit

/**
 Annotation class for showing rental bike location on MapKit
 */
class RentalBikeAnnotation : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var discipline: String?
    var title: String?
    var subtitle: String?
    var place: Place?
    
    required init(discipline: String, place: Place) {
        
        if (place.location != nil && place.location?.lat != nil && place.location?.lng != nil) {
            self.coordinate = CLLocationCoordinate2DMake(
                Double((place.location?.lat)!)!,
                Double((place.location?.lat)!)!)
        } else {
            self.coordinate = CLLocationCoordinate2DMake(0, 0)
        }
        
        self.discipline = discipline
        self.title = place.name
        self.subtitle = nil
        self.place = place
    }
}
