//
//  WebService.swift
//  Bike Rent
//
//  Created by Olcay Ertaş on 11/09/2017.
//  Copyright © 2017 Saitama. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class BikeRentService: NSObject {

    private static var BaseUrl = "https://localhost:3443/api/V2.0/"

    private static var Manager: Alamofire.SessionManager = {

        // Create the server trust policies
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "localhost": .disableEvaluation
        ]

        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
                configuration: URLSessionConfiguration.default,
                serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )

        return manager
    }()


    private static func registerOrLogin(method: HTTPMethod, email: String, password: String, completion: @escaping (LoginOrRegisterModel?) -> Void) {

        let params: [String: String] = ["email": email, "password": password]
        let url = BaseUrl + "users/"

        Manager.request(url, method: method, parameters: params, encoding: JSONEncoding.default)
                .validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"])
                .responseString { response in
                    //debugPrint(response)
                    switch response.result {
                    case .success:
                        print("WebService : registerOrLogin : Validation Successful")

                        if let json = response.result.value {
                            let result = Mapper<LoginOrRegisterModel>().map(JSONString: json)!
                            completion(result)
                        } else {
                            completion(nil)
                        }

                    case .failure(let error):
                        print(error)
                        completion(nil)
                    }
                }
    }


    public static func register(email: String, password: String, completion: @escaping (LoginOrRegisterModel?) -> Void) {
        BikeRentService.registerOrLogin(method: .put, email: email, password: password, completion: completion)
    }


    public static func login(email: String, password: String, completion: @escaping (LoginOrRegisterModel?) -> Void) {
        BikeRentService.registerOrLogin(method: .post, email: email, password: password, completion: completion)
    }


    public static func getLocations(completion: @escaping ([Place]?) -> Void) {

        let url = BaseUrl + "places/"

        Manager.request(url, method: .get)
                .validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"])
                .responseString { response in
                    //debugPrint(response)
                    switch response.result {
                    case .success:
                        print("WebService : getLocations : Validation Successful")

                        if let json = response.result.value {
                            let result = Mapper<Places>().map(JSONString: json)
                            completion(result?.places)
                        } else {
                            completion(nil)
                        }

                    case .failure(let error):
                        print(error)
                        completion(nil)
                    }
                }
    }


    public static func getPayments(completion: @escaping ([Payment]?) -> Void) {

        let url = BaseUrl + "payments/"

        Manager.request(url, method: .get)
                .validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"])
                .responseString { response in
                    //debugPrint(response)
                    switch response.result {
                    case .success:
                        print("WebService : getPayments : Validation Successful")

                        if let json = response.result.value {
                            let result = Mapper<Payment>().mapArray(JSONString: json)
                            completion(result)
                        } else {
                            completion(nil)
                        }

                    case .failure(let error):
                        print(error)
                        completion(nil)
                    }
                }
    }


    public static func createPayment(placeId: String, card: CreditCard, completion: @escaping (BaseResponse?) -> Void) {

        let url = BaseUrl + "payments/"
        let parameters: [String: String] = [
            "placeId": placeId,
            "number": card.number!,
            "name": card.name!,
            "cvv": card.cvv!,
            "expiryMonth": card.expiryMonth!,
            "expiryYear": card.expiryYear!]
        let token = UserDefaults.standard.string(forKey: "token")
        let headers: HTTPHeaders = ["Authorization": token!]

        Manager.request(url, method: .put, parameters: parameters, headers: headers)
                .validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"])
                .responseString { response in
                    //debugPrint(response)
                    switch response.result {
                    case .success:
                        print("WebService : createPayment : Validation Successful")

                        if let json = response.result.value {
                            let result = Mapper<BaseResponse>().map(JSONString: json)
                            completion(result)
                        } else {
                            completion(nil)
                        }

                    case .failure(let error):
                        print(error)
                        completion(nil)
                    }
                }
    }
}




