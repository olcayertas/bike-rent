//
//  Models.swift
//  Bike Rent
//
//  Created by Olcay Ertaş on 10/09/2017.
//  Copyright © 2017 Saitama. All rights reserved.
//

import Foundation
import ObjectMapper
import MapKit

enum ResponseCode : Int {
    case Unauthorized           =  401
    case InvalidJson            =  422
    case EmailAlreadyTaken      = 1000
    case UserNotFound           = 1001
    case PlaceNotFound          = 1002
    case InvalidCreditCard      = 1003
    case PaymentSuccess         = 1004
    case MissingRequiredField   = 1005
}

class BaseResponse: Mappable {
    
    /**
     Response Codes and Messages:
     - 1000 - EmailAlreadyTaken
     - 1001 - UserNotFound
     - 1002 - PlaceNotFound
     - 1003 - InvalidCreditCard
     - 1004 - PaymentSuccess
     -  401 - Unauthorized
     -  422 - InvalidJson
     */
    var message : String?
    var code : Int?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message     <- map["message"]
        code        <- map["code"]
    }
}


/**
 
 Model for both login end register responses.
 
 */
class LoginOrRegisterModel: BaseResponse {
    
    /**
        Token string on success
     */
    var token : String?
    
    required init?(map: Map) {
        super.init(map:map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        token <- map["token"]
    }
}


/**
    Geo location.
 */
class Location : Mappable {
    var lat : String?
    var lng : String?
    
    required init?(map: Map) {}

    required init(lat: String, lng: String) {
        self.lat = lat;
        self.lng = lng;
    }
    
    func mapping(map: Map) {
        lat     <- map["lat"]
        lng     <- map["lng"]
    }
}


/**
 Rental location
 */
class Place : Mappable {
    var updatedAt : String?
    var createdAt : String?
    var id : String?
    var name : String?
    var location: Location?
    
    required init?(map: Map) {}

    required init(
        updatedAt : String,
        createdAt : String,
        id : String,
        name : String,
        location : Location) {
        self.updatedAt = updatedAt;
        self.createdAt = createdAt;
        self.id = id;
        self.name = name
        self.location = location
    }
    
    func mapping(map: Map) {
        updatedAt     <- map["updatedAt"]
        createdAt     <- map["createdAt"]
        id            <- map["id"]
        name          <- map["name"]
        location      <- map["location"]
    }
}


class Places : Mappable {
    var places : [Place]?

    required init?(map: Map) {}

    func mapping(map: Map) {
        places <- map["places"]
    }
}


class CreditCard : Mappable{
    var number : String?
    var name : String?
    var cvv : String?
    var expiryMonth : String?
    var expiryYear : String?
    
    required init?() {}
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        number          <- map["number"]
        name            <- map["name"]
        cvv             <- map["cvv"]
        expiryMonth     <- map["expiryMonth"]
        expiryYear      <- map["expiryYear"]
    }
}


class Payment : Mappable {
    var updatedAt       : String?
    var createdAt       : String?
    var creditCard      : CreditCard?
    var email           : String?
    var placeId         : String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        updatedAt       <- map["updatedAt"]
        createdAt       <- map["createdAt"]
        creditCard      <- map["creditCard"]
        email           <- map["email"]
        placeId         <- map["placeId"]
    }
}





