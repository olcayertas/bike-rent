//
//  MapViewController.swift
//  Bike Rent
//
//  Created by Olcay Ertaş on 10/09/2017.
//  Copyright © 2017 Saitama. All rights reserved.
//

import UIKit
import MapKit
import ObjectMapper

class MapViewController: UIViewController, MKMapViewDelegate {
    
    let regionRadius: CLLocationDistance = 1000
    @IBOutlet var mapView : MKMapView?
    @IBOutlet var labelSelectedLocation : UILabel?
    @IBOutlet var buttonRentSelected : UIButton?
    var places: [Place]?
    var selectedPlace : Place?

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        mapView?.delegate = self;
        
        BikeRentService.getLocations(completion: { places in
            
            places?.forEach({ place in
                self.mapView?.addAnnotation(RentalBikeAnnotation(discipline: "", place: place))
            })
        })
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if let annotation = annotation as? RentalBikeAnnotation {
            
            let identifier = "pin"
            
            var view: MKPinAnnotationView
            
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
            }
            
            return view
        }
        
        return nil
    }

    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let annotation : RentalBikeAnnotation = view.annotation as! RentalBikeAnnotation
        labelSelectedLocation?.text = annotation.title;
        selectedPlace = annotation.place
    }


    @IBAction func rentSelected() {
        performSegue(withIdentifier: "RentSegue", sender: self)
    }
    
    @IBAction func showHistory() {
        performSegue(withIdentifier: "HistorySegue", sender: self)
    }
    
    @IBAction func logout() {
        UserDefaults.standard.removeObject(forKey: "token")
        UserDefaults.standard.synchronize()
        navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.destination.isKind(of: RentViewController.self)) {
            let destination = segue.destination as! RentViewController
            destination.place = selectedPlace
        }
    }
}
