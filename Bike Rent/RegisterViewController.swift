//
//  RegisterViewController.swift
//  Bike Rent
//
//  Created by Olcay Ertaş on 10/09/2017.
//  Copyright © 2017 Saitama. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet var textFieldMail : UITextField?
    @IBOutlet var textFieldPassword : UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }


    @IBAction func dismissSelf(sender: UIBarButtonItem) {
        dismissSelf()
    }

    func dismissSelf() {
        print("RegisterViewController : dismiss!")
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func register() {
        
        //TODO: Validate password strength
        
        if (textFieldMail?.text == "" || textFieldPassword?.text! == "") {
            showAlertView(controller: self, title: "Error", message: "Please fill all fields!", handler: { (alert, action) in
                alert.dismiss(animated: true, completion: nil)
            })
            return
        }
        
        if !isValidEmail(testStr:(textFieldMail?.text)!) {
            showAlertView(controller: self, title: "Error", message: "Please provide a valid e-mail!", handler: { (alert, action) in
                alert.dismiss(animated: true, completion: nil)
            })
            return
        }
        
        let email = textFieldMail?.text ?? ""
        let password = textFieldPassword?.text ?? ""
        
        BikeRentService.register(email: email, password: password) { model in
            
            if model?.token != nil {
                print("Register successful!");
                showAlertView(controller: self, title: "Success", message: "You have been registered! Press OK to go back to login screen.", handler: { (alert, action) in
                    alert.dismiss(animated: true, completion: nil)
                    self.dismissSelf()
                })
            } else {
                
                let title = "Error"
                
                if model?.code != nil {
                    let errorCode : Int = Int(model?.code ?? ResponseCode.InvalidJson.rawValue)
                    
                    switch errorCode {
                    case ResponseCode.EmailAlreadyTaken.rawValue:
                        showAlertView(controller: self, title: title, message: "This email is already taken!", handler: { (alert, action) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                        break;
                    default:
                        showAlertView(controller: self, title: title, message: "Failed to get response!", handler: { (alert, action) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                    }
                }
            }
        }
    }
}
