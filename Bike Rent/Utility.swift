//
//  Utility.swift
//  Bike Rent
//
//  Created by Olcay Ertaş on 10/09/2017.
//  Copyright © 2017 Saitama. All rights reserved.
//

import Foundation
import SwiftyJSON

func readJson(fileName: String) -> String? {
    
    if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
            return String(data: data, encoding: String.Encoding.utf8)
        } catch let error {
            print(error.localizedDescription)
            return nil;
        }
    }
    
    print("Utility : readJson : Invalid filename/path.")
    return nil;
}


func isValidEmail(testStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}


func showAlertView(controller: UIViewController, title: String, message: String, handler: ((UIAlertController, UIAlertAction) -> Void)? = nil) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
        handler!(alert, action)
    }))
    controller.present(alert, animated: true, completion: nil)
}
