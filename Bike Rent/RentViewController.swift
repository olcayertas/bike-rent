//
//  RentViewController.swift
//  Bike Rent
//
//  Created by Olcay Ertaş on 12/09/2017.
//  Copyright © 2017 Saitama. All rights reserved.
//

import UIKit

class RentViewController: UIViewController {

    var place: Place?
    @IBOutlet var labelPlace: UILabel?
    @IBOutlet var textFieldCardOwner: UITextField?
    @IBOutlet var textFieldCardNumber: UITextField?
    @IBOutlet var textFieldCardCVV: UITextField?
    @IBOutlet var textFieldMonth: UITextField?
    @IBOutlet var textFieldYear: UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()
        print("RentViewController : " + (place?.name)!)
        labelPlace?.text = place?.name
    }

    @IBAction func rent() {
        //TODO: Validate form
        //TODO: Use credit card form kits like PaymentKit and BKMoneyKit
        let card: CreditCard = CreditCard()!
        card.name = textFieldCardOwner?.text
        card.number = textFieldCardNumber?.text
        card.cvv = textFieldCardCVV?.text
        card.expiryMonth = textFieldMonth?.text
        card.expiryYear = textFieldYear?.text
        BikeRentService.createPayment(placeId: (place?.id)!, card: card, completion: { response in
            let responseCode = Int((response?.code)!)
            let title = "Error"

            switch responseCode {
            case ResponseCode.PaymentSuccess.rawValue:
                showAlertView(controller: self, title: title, message: "Payment successful! Go to rental location and take your bike with your ID", handler: { (alert, action) in
                    alert.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                })
                break
            case ResponseCode.PlaceNotFound.rawValue:
                showAlertView(controller: self, title: title, message: "Place not found! Please select an other location!", handler: { (alert, action) in
                    alert.dismiss(animated: true, completion: nil)
                })
                break
            case ResponseCode.InvalidCreditCard.rawValue:
                showAlertView(controller: self, title: title, message: "Invalid credit card information!", handler: { (alert, action) in
                    alert.dismiss(animated: true, completion: nil)
                })
                break
            case ResponseCode.Unauthorized.rawValue:
                showAlertView(controller: self, title: title, message: "Unauthorized! Please relogin!", handler: { (alert, action) in
                    alert.dismiss(animated: true, completion: nil)
                })
                break
            case ResponseCode.InvalidJson.rawValue:
                fallthrough
            default:
                showAlertView(controller: self, title: title, message: "Unknown error!", handler: { (alert, action) in
                    alert.dismiss(animated: true, completion: nil)
                })
                break
            }
        })
    }
}
