//
//  MockWebService.swift
//  Bike Rent
//
//  Created by Olcay Ertaş on 10/09/2017.
//  Copyright © 2017 Saitama. All rights reserved.
//

import UIKit
import ObjectMapper

let SampleToken = "eyJhbGciOiJub25lIiwidHlwIjoiSldUIn0.eyJpc3MiOiJodHRwczovL2p3dC1pZHAuZXhhbXBsZS5jb20iLCJzdWIiOiJtYWlsdG86bWlrZUBleGFtcGxlLmNvbSIsIm5iZiI6MTQ4MzQwOTk5NywiZXhwIjoxNDgzNDEzNTk3LCJpYXQiOjE0ODM0MDk5OTcsImp0aSI6ImlkMTIzNDU2IiwidHlwIjoiaHR0cHM6Ly9leGFtcGxlLmNvbS9yZWdpc3RlciJ9"

class MockWebService: NSObject {

    public static func login(email: String, password: String) -> LoginOrRegisterModel? {
        return MockWebService.loginOrRegister(requestType: "GET", email: email, password: password)
    }

    public static func register(email: String, password: String) -> LoginOrRegisterModel? {
        return MockWebService.loginOrRegister(requestType: "POST", email: email, password: password)
    }

    private static func loginOrRegister(requestType: String, email: String, password: String) -> LoginOrRegisterModel? {
        //TODO: Call actual web service when Docker image is ready
        return nil
    }

    public static func getRentalPlaces() -> [Place]? {
        //TODO: Call actual web service when Docker image is ready
        let jsonArrayString = readJson(fileName: "places");
        
        if jsonArrayString != nil {
            return Mapper<Place>().mapArray(JSONString: jsonArrayString!);
        }
        
        return nil;
    }
}
