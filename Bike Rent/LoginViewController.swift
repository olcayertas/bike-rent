//
//  ViewController.swift
//  Bike Rent
//
//  Created by Olcay Ertaş on 10/09/2017.
//  Copyright © 2017 Saitama. All rights reserved.
//

import UIKit
import Foundation

class LoginViewController: UIViewController {
    
    @IBOutlet var textFieldMail : UITextField?
    @IBOutlet var textFieldPassword : UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkToken()
    }


    @IBAction func pushRegisterViewController(sender: UIBarButtonItem) {
        let viewController = RegisterViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func login() {
        
        if (textFieldMail?.text == "" || textFieldPassword?.text! == "") {
            showAlertView(controller: self, title: "Error", message: "Please fill all fields!", handler: { (alert, action) in
                alert.dismiss(animated: true, completion: nil)
            })
            return
        }
        
        if !isValidEmail(testStr:(textFieldMail?.text)!) {
            showAlertView(controller: self, title: "Error", message: "Please provide a valid e-mail!", handler: { (alert, action) in
                alert.dismiss(animated: true, completion: nil)
            })
            return
        }
        
        let email = textFieldMail?.text ?? ""
        let password = textFieldPassword?.text ?? ""
        
        BikeRentService.login(email: email, password: password) { model in
            
            if model?.token != nil {
                print("Login successful!");
                UserDefaults.standard.set(model?.token, forKey: "token")
                self.performSegue(withIdentifier: "MapViewControllerSegue", sender: self)
            } else {

                let title = "Error"

                if model?.code != nil {
                    let errorCode : Int = Int(model?.code ?? ResponseCode.InvalidJson.rawValue)
                    
                    switch errorCode {
                    case ResponseCode.Unauthorized.rawValue:
                        showAlertView(controller: self, title: title, message: "Email and password doesn't match!", handler: { (alert, action) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                        break;
                    case ResponseCode.InvalidJson.rawValue:
                        showAlertView(controller: self, title: title, message: "Failed to get response!", handler: { (alert, action) in
                            //TODO: Log this error to investigate. This should never happen.
                            alert.dismiss(animated: true, completion: nil)
                        })
                        break;
                    case ResponseCode.UserNotFound.rawValue:
                        showAlertView(controller: self, title: title, message: "No user is registered with this email!", handler: { (alert, action) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                        break;
                    default:
                        showAlertView(controller: self, title: title, message: "Failed to get response!", handler: { (alert, action) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                    }
                }
            }
        }
    }
    
    func checkToken() {
        let token = UserDefaults.standard.string(forKey: "token")
        
        if token != nil {
            self.performSegue(withIdentifier: "MapViewControllerSegue", sender: self)
        } else {
            print("No token available. Login required!")
        }
    }
}

